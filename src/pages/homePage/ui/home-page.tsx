import React from "react"

import "./styles/home-page.css"
import logo from "./icons/logo.svg"
import { HOME_PAGE } from "@shared/config/routes"

const HomePage = () => {
  return (
    <div className="App bg-cyan-950">
      current path: {HOME_PAGE}
      <header className="App-header bg-red-500">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  )
}

export default HomePage
