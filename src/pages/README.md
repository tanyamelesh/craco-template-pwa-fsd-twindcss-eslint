# For what?

This folder contains the route components for each page in the app, mostly composition, hardly any logic.

All folders should have `ui` folder and `model` as optional, for example, to use some store managers and describe the
model inside.
