import React from "react"
import { render, screen } from "@testing-library/react"
import NotFoundPage from "../ui"

test("renders learn react link", () => {
  render(<NotFoundPage />)
  const linkElement = screen.getByText(/not found/i)
  expect(linkElement).toBeInTheDocument()
})
