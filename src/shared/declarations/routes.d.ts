type RouteName = string

interface RouteDescription {
  path: RouteName
  component: import("react").ComponentType
  layout?: import("react").ComponentType<import("react").PropsWithChildren>
}
