# For what?

This folder contains the interactivity of the card (e.g., like button) and the logic of processing those interactions.

Here you should to store files which describes some actions just like verbs in your app. business entities. (e.g., User,
Product, Order, look [here](https://feature-sliced.design/docs/get-started/overview))
