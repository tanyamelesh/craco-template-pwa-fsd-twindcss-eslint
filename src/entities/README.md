# For what?

The folder contains the shell of the card with slots for content and the interactive elements. The tile representing the
post author is also here, but in a different slice.

Here you should to store files which describes some entities just like noun of your app. business entities. (e.g., User,
Product, Order, look [here](https://feature-sliced.design/docs/get-started/overview))

Structure of one entity, for example, `./src/entities/user`:

- `ui` - required
- `model` - required
- `lib` - optional
- `api` - optional
