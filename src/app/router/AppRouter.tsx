import React from "react"
import { Route, Routes } from "react-router-dom"
import map from "lodash/map"

import { HOME_PAGE } from "@shared/config/routes"
import Index from "@pages/notFoundPage/ui"
import HomePage from "@pages/homePage/ui"

const routes: RouteDescription[] = [
  {
    path: HOME_PAGE,
    component: HomePage,
  },
]

export const AppRouter = () => {
  return (
    <Routes>
      {map(routes, ({ path, component: Component }) => (
        <Route key={path} path={path} element={<Component />} />
      ))}
      <Route path="*" element={<Index />} />
    </Routes>
  )
}
