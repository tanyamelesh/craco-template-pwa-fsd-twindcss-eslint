This template based on "[cra-template-pwa-typescript](https://github.com/cra-template/pwa)" (by Google LLC).

## About

You can use this project as "blank" template with FSD for new projects.

There are settings for:

- Typescript
- PWA
- CRACO
- Aliases
- Storybook
- Eslint
- Prettier
- TailwindCSS
