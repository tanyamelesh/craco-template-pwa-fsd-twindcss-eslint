const config = {
  env: {
    browser: true,
    node: true,
    es2020: true,
  },
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "prettier", "lodash", "jsx-a11y", "unused-imports"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "plugin:lodash/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:storybook/recommended",
  ],
  rules: {
    "no-console": "error",
    "prettier/prettier": "error",
    "@typescript-eslint/no-explicit-any": "error",
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "jsx-a11y/anchor-is-valid": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "unused-imports/no-unused-imports": "error",
    "unused-imports/no-unused-vars": [
      "warn",
      {
        argsIgnorePattern: "^_",
        varsIgnorePattern: "^_",
        caughtErrorsIgnorePattern: "^_",
      },
    ],
  },
  settings: {
    react: {
      version: "18",
    },
  },
  overrides: [
    {
      files: [
        "src/**/__tests__/**/*.test.{tsx,ts}",
        "src/**/__tests_utils__/**/*.{tsx,ts}",
        "src/**/stories/**/*.stories.tsx",
        "src/**/__mocks__/**/*.{tsx,ts}",
      ],
      rules: {
        "@typescript-eslint/no-empty-function": "off",
        "lodash/prefer-noop": "off",
      },
    },
    {
      files: ["./*.js"],
      rules: {
        "@typescript-eslint/no-var-requires": "off",
      },
    },
    {
      files: ["./src/features/pwa/service-worker.ts", "./src/features/pwa/serviceWorkerRegistration.ts", "./src/reportWebVitals.ts"],
      rules: {
        "lodash/prefer-lodash-method": "off",
        "no-console": "off",
        "lodash/prefer-includes": "off",
        "lodash/prefer-lodash-typecheck": "off",
      },
    },
    {
      files: ["src/**/*.{tsx,ts}"],
      rules: {
        "react/no-unknown-property": [
          "error",
          {
            ignore: ["jsx", "global"],
          },
        ],
      },
    },
  ],
  ignorePatterns: ["**/node_modules", "**/yarn.lock", "**/build", "**/dist", "**/lib"],
}

export default config
