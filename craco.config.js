const aliases = require("./aliases")
const appPackageConfig = require("./package.json")

const reduce = require("lodash/reduce")

const jestAliases = reduce(
  aliases,
  (res, path, name) => {
    res[`^${name}(.*)$`] = `${path}$1`
    return res
  },
  {},
)

module.exports = {
  webpack: {
    alias: { ...aliases },
  },
  jest: {
    configure: {
      clearMocks: true,
      moduleFileExtensions: ["ts", "tsx", "js", "json"],
      maxWorkers: 2,
      coverageThreshold: {
        global: {
          statements: 90,
          branches: 90,
          functions: 90,
          lines: 90,
        },
      },
      coverageReporters: ["html"],
      collectCoverage: true,
      preset: "ts-jest/presets/js-with-ts",
      testEnvironment: "node",
      displayName: appPackageConfig.name,
      testMatch: ["<rootDir>/src/**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)"],
      moduleNameMapper: {
        ...jestAliases,
      },
    },
  },
}
