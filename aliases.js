const path = require("path")
const resolvePath = (p) => path.resolve(__dirname, p)

module.exports = {
  "@shared": resolvePath("./src/shared"),
  "@features": resolvePath("./src/features"),
  "@pages": resolvePath("./src/pages"),
}
